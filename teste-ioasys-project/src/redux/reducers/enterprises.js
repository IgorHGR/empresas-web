import { ADD_ENTERPRISES } from '../actions/actionTypes';

const initialState = {
  allIds: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case ADD_ENTERPRISES: {
      return {
        ...state,
        allIds: [...state.allIds, action.payload.id],
      };
    }
    default:
      return state;
  }
}
