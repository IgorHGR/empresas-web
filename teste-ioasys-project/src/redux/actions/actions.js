import { ADD_ENTERPRISES } from './actionTypes';

export const addEnterprises = (id) => ({
  type: ADD_ENTERPRISES,
  payload: {
    id: id,
  },
});
