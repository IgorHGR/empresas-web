import { createStore } from 'redux';
import rootReducer from './reducers/enterprises';

export default createStore(rootReducer);
