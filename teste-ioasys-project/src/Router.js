import React from 'react';
import Index from './pages/index';
import About from './pages/about';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

function IndexRouter() {
  return (
    <Router>
      <Switch>
        <Route exact path="/enterprises" component={About} />
        <Route exact path="/" component={Index} />
      </Switch>
    </Router>
  );
}

export default IndexRouter;
