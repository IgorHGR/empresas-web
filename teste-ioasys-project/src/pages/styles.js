const styles = {
  container: {
    backgroundColor: '#ebe9d7',
    height: '45rem',
  },
  imgSize: { width: 300, height: 70, marginTop: -20 },
  colInput: {
    backgroundColor: 'transparent',
    textAlign: 'center',
    marginTop: '5rem',
  },
  welcomeContainer: {
    justifyContent: 'center',
    marginTop: 70,
    paddingLeft: 20,
  },
  welcomeText: {
    textAlign: 'center',
    width: 160,
    color: '#383743',
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: 22,
    backgroundColor: 'transparent',
  },
  loremIpsum: {
    fontSize: '1.125rem',
    fontFamily: 'Roboto',
  },
  centralizeRow: { justifyContent: 'center' },
  loginContainer: {
    justifyContent: 'center',
    backgroundColor: 'transparent',
    paddingLeft: 60,
    marginRight: 20,
    marginTop: -20,
  },
  loginBtn: {
    width: '100%',
    height: 50,
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    fontSize: '1.125rem',
    textAlign: 'center',
  },
};

export default styles;
