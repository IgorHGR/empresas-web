import React, { useEffect, useState, useRef } from 'react';
import { getEnterprisesState } from '../redux/selectors';
import { connect } from 'react-redux';
import enterprises from '../redux/reducers/enterprises';
import { Container, Col, Row, Collapse } from 'reactstrap';
import { TextInput } from 'react-materialize';
import $ from 'jquery';
import enterprisesList from '../enterprises';
import api from '../config/api';
import { BiSearch } from 'react-icons/bi';
import { FaExpandAlt } from 'react-icons/fa';
const About = (props) => {
  const [enterpriseNames, setEnterprises] = useState([]);
  const [suggestions, setSuggestions] = useState([]);
  const [rendered, setRendered] = useState([]);
  const [height, setHeight] = useState(window.innerHeight);
  const [width, setWidth] = useState(window.innerWidth);
  const [colSize1, setColSize1] = useState(6);
  const [colSize2, setColSize2] = useState(6);
  const [listOfEnterprisesNames, setListOfEnterprisesNames] = useState([]);
  const [enterpriseSelected, setEnterpriseSelected] = useState('');
  const [test, setTest] = useState('');
  const enterprisesNamesList = [];
  const [loginEmail, setLoginEmail] = useState('testeapple@ioasys.com.br');
  const [loginPassword, setPassword] = useState('12341234');
  const [enterpriseId, setEnterpriseId] = useState('');
  const [shouldShow, setShow] = useState(true);
  const [showDescription, setDescription] = useState(false);
  const [enterpriseToShow, setToShow] = useState({});
  const [typeEnterprise, setType] = useState('');
  const [countryEnterprise, setCountry] = useState('');
  const [expand, setExpand] = useState(true);
  const [open, setOpen] = useState(true);
  let params = {
    email: loginEmail,
    password: loginPassword,
  };

  const fetchEnterprise = async (id) => {
    setEnterpriseId(id);
    // const enterpriseToBeShown = [];
    await api.get('/enterprises/' + id).then(async (response) => {
      setToShow(response.data.enterprise);
      setType(response.data.enterprise.enterprise_type.enterprise_type_name);
      setCountry(response.data.enterprise.country);
      api.defaults.headers.common['access-token'] = await response.headers[
        'access-token'
      ];
      api.defaults.headers.common.client = await response.headers.client;
      api.defaults.headers.common.uid = await response.headers.uid;
    });
  };
  const handleKey = (key) => {
    if ((key = 13)) {
      enterprisesList.forEach(function (enterprise) {
        if (enterprise.name === enterpriseSelected) {
          fetchEnterprise(enterprise.id);
        }
      });
    }
  };

  const textInputRef = useRef(null);

  useEffect(() => {
    textInputRef.current.focus();
    console.log(height);
    console.log(width);
    // let inputEnterprise = document.querySelector('#input-enterprise');
    // inputEnterprise.addEventListener('keyup', handleKeyUp);

    // $('.box-enterprise').click(function () {
    //   $(this)
    //     .children('.transition-div')
    //     .animate({ height: '500px', width: '102%' });
    // });

    $('.expand-minimize').click(function () {
      $('.transition-div').toggleClass('expand');
      $('.transition-div-2').toggleClass('collapse');
      $('font-content').toggleClass('expand');
      $('.box-enterprise').toggleClass('expand');
      // console.log('minimiza');
      // $('.transition-div').animate({ height: '13.273rem', width: '50%' }, 2000);
      // $('.transition-div-2').animate({ width: '30%', height: '10rem' }, 2000);
    });
    // setEnterprises(enterprises.push(setNames()));
    getNames();
  }, []);

  async function getNames() {
    let list = [];
    enterprisesList.forEach(function (enterprise) {
      console.log(enterprise.name);
      list.push(enterprise.name);
    });
    setListOfEnterprisesNames(list);
  }

  const onTextChanged = (e) => {
    setShow(true);
    const value = e.target.value;
    setEnterpriseSelected(value);
    console.log(e, 'charcode');
    let suggestions = [];
    if (value.length > 0) {
      const regex = new RegExp(`${value}`, 'i');
      suggestions = listOfEnterprisesNames
        .sort()
        .filter((name) => regex.test(name));
    }
    setSuggestions(suggestions);
  };

  const showSuggestions = () => {
    if (shouldShow) {
      return suggestions.map((name) => (
        <li className="list-item" onClick={() => getEnterprise(name)}>
          {name}
        </li>
      ));
    }
    return null;
  };

  const getEnterprise = (name) => {
    setShow(false);
    setEnterpriseSelected(name);
    textInputRef.current.focus();
    // props.enterprises.allIds[0].forEach(function (enterprise) {
    //   if (enterprise.enterprise_name === name) {
    //     setRendered(rendered.push(enterprise));
    //     console.log(rendered);
    //   }
    // });
  };
  // const renderEnterprises = () => {
  //   if (enterprises.length === 0) {
  //     return null;
  //   }
  //   return (
  //     <div className="AutoCompleteText">
  //       <ul style={{ marginTop: -20 }}>
  //         {suggestions.map((item) => (
  //           <li
  //             style={{ fontFamily: 'Montserrat' }}
  //             onClick={() => suggestionSelected(item)}
  //           >
  //             {item}
  //           </li>
  //         ))}
  //       </ul>
  //     </div>
  //   );
  // };
  // console.log(enterprisesList);
  // console.log(listOfEnterprisesNames, 'lista');
  console.log(enterpriseToShow);
  return (
    <Container
      className="conteudo"
      style={{
        backgroundColor: '#ebe9d7',
        height: height + 200,
        width: width,
      }}
      fluid
    >
      <Row style={{ width: width + 5, marginLeft: -20 }} className="input-row">
        <Col>
          <div className="AutoCompleteText">
            <ul className="list">
              {/* <TextInput
                id="input-enterprise"
                inputClassName="TextInput-4"
                onChange={onTextChanged}
                email
                error="E-mail inválido"
                label="Pesquisar por nome da empresa"
                success="Great"
                value={enterpriseSelected}
              /> */}
              <div className="input-field col s6">
                <i class="material-icons prefix">
                  <BiSearch color="white"></BiSearch>
                </i>
                <input
                  className="input-field"
                  color="white"
                  ref={textInputRef}
                  onKeyPressCapture={handleKey}
                  onChange={onTextChanged}
                  type="text"
                  value={enterpriseSelected}
                />
                <label style={{ color: 'white' }} for="enterprise">
                  Nome da empresa
                </label>
              </div>
              {/* {suggestions.map((name) => (
                <li className="list-item" onClick={() => getEnterprise(name)}>
                  {name}
                </li>
              ))} */}
              {showSuggestions()}
            </ul>
          </div>
        </Col>
      </Row>
      <Row
        style={{
          justifyContent: 'center',
          backgroundColor: 'transparent',
          marginTop: -20,
        }}
      >
        <Col lg={12} md={12} sm={12} style={{ backgroundColor: '#ebe9d7' }}>
          <Row
            // onClick={() => setDescription(!showDescription)}
            style={{ justifyContent: 'center' }}
          >
            <Col>
              {/* <Row
                className="box-enterprise"
                style={{ alignItems: 'center', height: '100%' }}
              >
                <Col
                  lg={colSize1}
                  className="transition-div"
                  style={{ backgroundColor: 'green', height: 160, width: 293 }}
                >
                  <div>Teste</div>
                </Col>
                <Col
                  lg={6}
                  className="transition-div-2"
                  style={{ height: 160, width: 293, backgroundColor: 'pink' }}
                >
                  <Row
                    style={{
                      flexDirection: 'column',
                      height: '100%',
                      justifyContent: 'center',
                      marginLeft: 20,
                      paddingBottom: 50,
                    }}
                  >
                    <span style={{ fontSize: 27 }}>Empresa</span>
                    <span style={{ fontSize: 20 }}>Empresa</span>
                    <span style={{ fontSize: 17 }}>Empresa</span>
                  </Row>
                </Col>
              </Row> */}

              <Row
                className={'box-enterprise'}
                // style={{
                //   backgroundColor: 'white',
                //   marginTop: 50,
                //   width: '100%',
                //   height: 200,
                //   flexDirection: 'row',
                //   display: 'flex',
                // }}
              >
                <div className="transition-div">
                  <img
                    src={
                      enterpriseId
                        ? `https://empresas.ioasys.com.br/uploads/enterprise/photo/${enterpriseId}/240.jpeg`
                        : null
                    }
                    className="image-enterprise"
                    // style={{
                    //   marginTop: 20,
                    //   width: '70%',
                    //   height: '10rem',
                    //   backgroundColor: '#7dc075',
                    // }}
                  ></img>
                  {showDescription && (
                    <span style={{ width: '80%', marginTop: 10 }}>
                      {enterpriseToShow.description}
                    </span>
                  )}
                </div>
                <div
                  className="transition-div-2"
                  // style={
                  // {
                  // marginTop: 20,
                  // width: '30%',
                  // height: '90%',
                  // alignItems: 'center',
                  // justifyContent: 'flex-start',
                  // }
                  // }
                >
                  <div
                    className="content-enterprise"
                    // style={{ backgroundColor: 'orange' }}

                    // style={{
                    //   marginTop: 10,
                    //   width: '50%',
                    //   height: '60%',
                    //   backgroundColor: 'transparent',
                    //   flexDirection: 'column',
                    //   display: 'flex',
                    //   alignItems: 'flex-start',
                    // }}
                  >
                    <span className="Empresa-1">
                      {enterpriseToShow.enterprise_name}
                    </span>

                    <span className="Negocio">{typeEnterprise}</span>
                    <span className="Brasil">{countryEnterprise}</span>
                  </div>
                </div>
                <div
                  onClick={() => setDescription(!showDescription)}
                  className="expand-minimize"
                  style={{
                    position: 'absolute',
                    width: '3%',
                    height: '20%',
                    flexDirection: 'row',
                    display: 'flex',
                    backgroundColor: 'transparent',
                    marginLeft: 20,
                  }}
                >
                  <span>
                    <FaExpandAlt></FaExpandAlt>
                  </span>
                  <span style={{ marginLeft: 20 }}>Descrição</span>
                </div>
              </Row>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
};
console.log(enterprises);
const mapStateToProps = (state) => {
  const enterprises = getEnterprisesState(state);

  return { enterprises };
};

export default connect(mapStateToProps, null)(About);
