import React, { useState } from 'react';
import { Container, Col, Row, Button } from 'reactstrap';
import 'materialize-css';
import { TextInput } from 'react-materialize';
import { BiLockOpen } from 'react-icons/bi';
import { GoMail } from 'react-icons/go';
import styles from './styles';
import './styles.css';
import api from '../config/api';
import { connect } from 'react-redux';
import { addEnterprises } from '../redux/actions/actions';
import { getEnterprisesState } from '../redux/selectors';
import LoadingOverlay from 'react-loading-overlay';
import ClipLoader from 'react-spinners/ClipLoader';
import { Redirect } from 'react-router-dom';

const Index = (props) => {
  const [loginEmail, setLoginEmail] = useState('testeapple@ioasys.com.br');
  const [loginPassword, setPassword] = useState('12341234');
  const [spinner, setSpinner] = useState(false);
  const [redirect, setRedirect] = useState(false);
  const getEnterpriseUrl = (id) => {
    return `https://empresas.ioasys.com.br/api/v1/enterprises/${id}`;
  };

  // const fetchEnterprise = async (response) => {
  //   for (let i = 0; i < 100; i++) {
  //     enterprisePromises.push(
  //       fetch(getEnterpriseUrl(i)).then((response) => response.json())
  //     );
  //   }
  //   Promise.all(enterprisePromises).then((response) => console.log(response));
  // };
  let params = {
    email: loginEmail,
    password: loginPassword,
  };

  const enterprises = [];
  const fetchEnterPrise = async () => {
    // for (let i = 0; i < 58; i++) {
    // for (let i = 0; i < 58; i++) {
    await api.post('/users/auth/sign_in', params).then(async (response) => {
      console.log(response);
      api.defaults.headers.common['access-token'] = await response.headers[
        'access-token'
      ];
      api.defaults.headers.common.client = await response.headers.client;
      api.defaults.headers.common.uid = await response.headers.uid;
      // api
      //   .get('/enterprises/' + i)
      //   .then((response) => enterprises.push(response.data.enterprise));
    });
    // }
    props.addEnterprises(enterprises);
    setSpinner(false);
    setRedirect(true);
  };

  const requestLogin = async () => {
    setSpinner(true);
    await api.post('./users/auth/sign_in', params).then(async (response) => {
      if (response.data.success) {
        fetchEnterPrise();
      }
    });
  };
  console.log(props.enterprises.allIds[0]);

  if (redirect) {
    return <Redirect push to={'/enterprises/'} />;
  }

  const fs = require('fs');
  const path = require('path');

  const componentFolder = './src/components/';
  const componentJsonPath = './docs/components.json';

  const componentDataArray = [];

  function pushComponent(component) {
    componentDataArray.push(component);
  }

  function createComponentFile() {
    console.log(componentDataArray);
    const componentJsonArray = JSON.stringify(componentDataArray, null, 2);
    fs.writeFile(componentJsonPath, componentJsonArray, 'utf8', (err, data) => {
      if (err) {
        throw err;
      }
      console.log('Created component file');
    });
  }

  return (
    <LoadingOverlay
      active={spinner}
      spinner={
        <ClipLoader text size={30} color={'rgb(87, 187, 188)'}></ClipLoader>
      }
    >
      <Container fluid style={styles.container}>
        <Row>
          <Col style={styles.colInput} lg={{ size: 4, offset: 4 }}>
            <Row style={{ justifyContent: 'center' }}>
              <img
                style={styles.imgSize}
                src={require('../assets/logo-home@2x.png')}
              />
            </Row>
            <Row style={styles.welcomeContainer}>
              <span style={styles.welcomeText}>BEM-VINDO AO EMPRESAS</span>
            </Row>
            <Row>
              <span style={styles.loremIpsum}>
                Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc
                accumsan.
              </span>
            </Row>
            <Row>
              <TextInput
                email
                error="E-mail inválido"
                inputClassName="TextInput-4"
                label="Email"
                success="Great"
                validate
                value={loginEmail}
                icon={<GoMail color={'rgb(239, 87, 129)'} />}
                onChange={(e) => setLoginEmail(e.target.value)}
              />
            </Row>
            <Row style={{ marginTop: -30 }}>
              <TextInput
                password
                email
                error="Senha incorreta"
                id="TextInput-4"
                label="Senha"
                success="Great"
                validate
                value={loginPassword}
                onChange={(e) => setPassword(e.target.value)}
                icon={<BiLockOpen color={'rgb(239, 87, 129)'} />}
              />
            </Row>
            <Row
              style={{
                justifyContent: 'center',
                paddingLeft: 30,
                paddingBottom: 20,
              }}
            ></Row>
            <Row style={styles.loginContainer}>
              <Button onClick={requestLogin} style={styles.loginBtn}>
                ENTRAR
              </Button>
            </Row>
          </Col>
        </Row>
      </Container>
    </LoadingOverlay>
  );
};

const mapStateToProps = (state) => {
  const enterprises = getEnterprisesState(state);

  return { enterprises };
};
// const mapDispatchToProps = (dispatch) => {
//   return {
//     setEnterprises: (enterprises) => dispatch(setEnterprises(enterprises)),
//   };
// };
export default connect(mapStateToProps, { addEnterprises })(Index);
